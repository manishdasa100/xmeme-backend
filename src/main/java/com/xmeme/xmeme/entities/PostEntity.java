package com.xmeme.xmeme.entities;

import java.time.LocalDate;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * This entity represents an meme post entity to be persisted in the collection "userPosts"
 */

@Document(collection = "userPosts")
@CompoundIndex(name = "name_url_caption", def = "{'name': 1, 'url': 1, 'caption': 1}", unique = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class PostEntity {

    @Transient
    public static final String SEQUENCE_NAME = "posts_sequence";
    
    @Id
    private long id;

    private String name;

    private String url;

    private String caption;

    private LocalDate dateOfPosting;
}
